import sys
import glob
import os
import subprocess

import numpy as np
import pandas as pd
from PIL import Image
from six.moves import cPickle as pickle

sys.path.extend(['../resources/scripts1/commons'])
import data_commons as dtc
import dl_commons as dlc
import viz_commons as vc
from viz_commons import VisualizeDir, DiffParams, VisualizeStep

IM2LATEX_MODEL_PATH = '../resources/model1'
BATCH_SIZE = 10
TEMP_FOLDER = '../resources/result'


def make_seq_bins(df_):
    """
    Creates ndarrays of (padded) sequence bins from df_*_squashed / df_*_padded
    and pickles them as a dictionary of ndarrays wrapped in dataframes.
    This preprocessing is needed in order to quickly obtain an ndarray of
    token-sequences at training time.
    """
    bin_lens = df_.bin_len.unique()
    bins = {}
    bins_squashed = {}

    for len_ in bin_lens:
        df_slice = df_[df_.padded_seq_len == len_]
        bin_ = np.array(df_slice.padded_seq.values.tolist(), dtype=np.int32)
        bin_squashed = np.array(df_slice.squashed_seq.values.tolist(), dtype=np.int32)
        assert bin_.shape[1] == len_
        assert bin_.shape[0] == df_slice.shape[0]
        bins[len_] = pd.DataFrame(bin_, index=df_slice.index)
        bins_squashed[len_] = pd.DataFrame(bin_squashed, index=df_slice.index)

    return bins, bins_squashed


def preprocess_im2latex(input_path):
    data_dict = {
        'image': [],
        'height': [],
        'width': [],
        'word2id_len': [],
        u'bin_len': [],
        u'word2id': [],
        u'padded_seq': [],
        u'padded_seq_len': [],
        u'seq_len': [],
        u'squashed_len': [],
        u'squashed_seq': []
    }

    data_props = pd.read_pickle(os.path.join(IM2LATEX_MODEL_PATH, 'data_props.pkl'))

    max_length = 150
    Y = [data_props['word2id']['x']] * max_length
    padded_seq = Y + [0]
    padded_len = max_length + 1
    max_im_size = data_props['padded_image_dim']

    temp_files = os.listdir(input_path)

    for file in temp_files:
        if file.endswith(".png"):
            try:
                image = Image.open(os.path.join(input_path, file))
                old_size = image.size

                if old_size[0] <= max_im_size['width'] and old_size[1] <= max_im_size['height']:
                    data_dict['image'].append(file)
                    data_dict['height'].append(old_size[1])
                    data_dict['width'].append(old_size[0])
                    data_dict['word2id_len'].append(max_length)
                    data_dict['bin_len'].append(padded_len)
                    data_dict['word2id'].append(Y)
                    data_dict['padded_seq'].append(padded_seq)
                    data_dict['padded_seq_len'].append(padded_len)
                    data_dict['seq_len'].append(padded_len)
                    data_dict['squashed_len'].append(padded_len)
                    data_dict['squashed_seq'].append(padded_seq)

            except IOError:
                continue

    df_test_squashed = pd.DataFrame(data_dict)[
        ['image', 'height', 'width', 'word2id_len', u'bin_len', u'word2id', u'padded_seq', u'padded_seq_len',
         u'seq_len', u'squashed_len', u'squashed_seq']]

    df_train_squashed = pd.DataFrame(data_dict)[
        ['image', 'height', 'width', 'word2id_len', u'bin_len', u'word2id', u'padded_seq', u'padded_seq_len',
         u'seq_len', u'squashed_len', u'squashed_seq']]

    df_valid_squashed = pd.DataFrame(data_dict)[
        ['image', 'height', 'width', 'word2id_len', u'bin_len', u'word2id', u'padded_seq', u'padded_seq_len',
         u'seq_len', u'squashed_len', u'squashed_seq']]

    if df_test_squashed.shape[0] % BATCH_SIZE > 0:
        shortfall = BATCH_SIZE - (df_test_squashed.shape[0] % BATCH_SIZE)
        df_test_squashed = pd.concat([df_test_squashed] + [df_test_squashed.iloc[-1:]] * shortfall)

    bins_test, bins_sq_test = make_seq_bins(df_test_squashed)
    bins_train, bins_sq_train = make_seq_bins(df_train_squashed)
    bins_valid, bins_sq_valid = make_seq_bins(df_valid_squashed)

    with open(os.path.join(TEMP_FOLDER, 'batch_size.pkl'), 'wb') as f:
        pickle.dump(BATCH_SIZE, f, pickle.HIGHEST_PROTOCOL)

    df_test_squashed.to_pickle(os.path.join(TEMP_FOLDER, 'df_test.pkl'))
    df_train_squashed.to_pickle(os.path.join(TEMP_FOLDER, 'df_train.pkl'))
    df_valid_squashed.to_pickle(os.path.join(TEMP_FOLDER, 'df_valid.pkl'))

    for t in [('test', bins_test, bins_sq_test), ('train', bins_train, bins_sq_train),
              ('valid', bins_valid, bins_sq_valid)]:
        with open(os.path.join(TEMP_FOLDER, 'raw_seq_%s.pkl' % t[0]), 'wb') as f:
            pickle.dump(t[1], f, pickle.HIGHEST_PROTOCOL)
        with open(os.path.join(TEMP_FOLDER, 'raw_seq_sq_%s.pkl' % t[0]), 'wb') as f:
            pickle.dump(t[2], f, pickle.HIGHEST_PROTOCOL)

    data_props['MaxSeqLen'] = df_test_squashed.padded_seq_len.max()
    with open(os.path.join(TEMP_FOLDER, 'data_props.pkl'), 'wb') as f:
        pickle.dump(data_props, f, pickle.HIGHEST_PROTOCOL)


def start_im2latex(input_path):
    try:
        preprocess_im2latex(input_path)

        cmd = [
            'python2', 'scripts1/run.py',
            '-a', '0.0001',
            '-e', '-1',
            '-b', str(BATCH_SIZE),
            '-v', '-1',
            '-i', '2',
            '--r-lambda', '0.00005',
            '--raw-data-folder', TEMP_FOLDER,
            '--test',
            '--save-all-eval',
            '--restore', 'model1',
            '--image-folder', input_path
        ]

        wd = os.getcwd()
        os.chdir('../resources')
        subprocess.call(cmd)

        os.chdir(wd)

        preds_vd = VisualizeDir(os.path.join(IM2LATEX_MODEL_PATH, 'store'))
        preds_vs = VisualizeStep(preds_vd, 'test', 227600)
        df = preds_vs.strs(
            key='predicted_ids',
            trim=True,
            sortkey=None,
            keys=['image_name'],
            sort_ascending=False,
            wrap_strs=False
        )

        return df

    finally:
        for file in glob.glob('../resources/result/*'):
            os.remove(file)

        for file in glob.glob(os.path.join(IM2LATEX_MODEL_PATH, 'store/*')):
            os.remove(file)
        os.rmdir(os.path.join(IM2LATEX_MODEL_PATH, 'store'))

        for file in glob.glob(os.path.join(IM2LATEX_MODEL_PATH, 'events.out.*')):
            os.remove(file)
