import glob
import os
import subprocess
import sys
import csv
import datetime
from enum import Enum

import PIL
import design
import numpy as np
from PIL import Image
from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSignal, QThread

import Im2LatexBindings

MAX_WIDTH = 500
MAX_HEIGHT = 160
BUCKETS = [[240, 100], [320, 80], [400, 80], [400, 100], [480, 80], [480, 100], [560, 80], [560, 100], [640, 80],
           [640, 100], [720, 80], [720, 100], [720, 120], [720, 200], [800, 100], [800, 320], [1000, 200], [1000, 400],
           [1200, 200], [1600, 200], [1600, 1600]]
DEFAULT_SCALE = 2


def show_error(error):
    error_dialog = QtWidgets.QMessageBox()
    error_dialog.setIcon(QtWidgets.QMessageBox.Critical)
    error_dialog.setText(error)
    error_dialog.setWindowTitle('Error')
    error_dialog.exec_()


def crop_image(image):
    temp_image = image.convert('L')
    image_data = np.asarray(temp_image, dtype=np.uint8)
    nnz_inds = np.where(image_data != 255)

    if len(nnz_inds[0]) == 0:
        return image

    y_min = np.min(nnz_inds[0])
    y_max = np.max(nnz_inds[0])
    x_min = np.min(nnz_inds[1])
    x_max = np.max(nnz_inds[1])
    image = image.crop((x_min, y_min, x_max + 1, y_max + 1))
    return image


def pad_image(image):
    pad_top, pad_left, pad_bottom, pad_right = [8, 8, 8, 8]
    old_size = (image.size[0] + pad_left + pad_right, image.size[1] + pad_top + pad_bottom)
    j = -1
    for i in range(len(BUCKETS)):
        if old_size[0] <= BUCKETS[i][0] and old_size[1] <= BUCKETS[i][1]:
            j = i
            break
    if j < 0:
        new_size = old_size
        new_im = Image.new("RGB", new_size, (255, 255, 255))
        new_im.paste(image, (pad_left, pad_top))
        return new_im

    new_size = BUCKETS[j]
    new_im = Image.new("RGB", new_size, (255, 255, 255))
    new_im.paste(image, (pad_left, pad_top))
    return new_im


def scale_image(image, scale):
    old_size = image.size
    new_size = (int(old_size[0] / scale), int(old_size[1] / scale))
    image = image.resize(new_size, PIL.Image.LANCZOS)
    return image


class Method(Enum):
    Im2Markup = 1
    Im2Latex = 2


class Worker(QThread):
    write_to_log_signal = pyqtSignal(str)
    set_editable_signal = pyqtSignal(bool)

    def __init__(self, method, input_path, output_path, downscale):
        super(Worker, self).__init__()
        self.file_name_dict = {}
        self.method = method
        self.input_path = input_path
        self.output_path = output_path
        self.downscale = downscale

    def set_editable(self, flag):
        self.set_editable_signal.emit(flag)

    def log(self, text):
        self.write_to_log_signal.emit(text)

    def run(self):
        self.set_editable(False)
        self.log('Working directory = %s, Output directory = %s, method = %s'
                 % (self.input_path, self.output_path, self.method.name))

        if self.method == Method.Im2Markup:
            self.start_im2markup()
        if self.method == Method.Im2Latex:
            self.start_im2latex()

        self.log('Process finished!')
        self.set_editable(True)

    def start_im2latex(self):
        try:
            i = self.preprocess()
            if i == 0:
                self.log('Zero images were found eligible for processing!')
                return

            df = Im2LatexBindings.start_im2latex('../resources/images/')

            df = df[['image_name', 'predicted_ids']] \
                .rename(columns={'predicted_ids': 'latex'}) \
                .drop_duplicates(subset=['image_name'])

            output_file_path = os.path.join(self.output_path, 'results.csv')

            with open(output_file_path, 'w') as output_file:
                writer = csv.writer(output_file)
                for index, row in df.iterrows():
                    file_name = self.file_name_dict[row['image_name']]
                    writer.writerow([file_name, row['latex']])

                self.log('Results written to results.csv file!')

        finally:
            self.log('Cleaning up temp files...')
            temp_files = glob.glob('../resources/images/*')
            for file in temp_files:
                os.remove(file)

    def start_im2markup(self):
        try:
            i = self.preprocess()
            if i == 0:
                self.log('Zero images were found eligible for processing!')
                return

            self.log('Started recognition. This might take a while...')

            cmd = [
                'th', 'scripts/train.lua',
                '-phase', 'test',
                '-gpu_id', '1',
                '-visualize',
                '-load_model', '-model_dir', 'model0',
                '-data_base_dir', 'images',
                '-log_path', 'logs',
                '-data_path', 'model0/filter.lst',
                '-label_path', 'model0/formula.lst',
                '-output_dir', 'result',
                '-max_num_tokens', '150',
                '-max_image_width', '500',
                '-max_image_height', '160',
                '-batch_size', '5'
            ]

            wd = os.getcwd()
            os.chdir('../resources')
            subprocess.call(cmd)

            self.log('Finished recognition. Saving result')
            output_file_path = os.path.join(self.output_path, 'results.csv')

            with open('result/results.txt', 'r') as result_file, open(output_file_path, 'w') as output_file:
                result = result_file.read().split('\n')
                writer = csv.writer(output_file)

                for line in result:
                    if line:
                        tokens = line.split('\t')
                        file_name = self.file_name_dict[tokens[0]]
                        answer = tokens[2]
                        writer.writerow([file_name, answer])

                self.log('Results written to results.csv file!')

            os.chdir(wd)

        finally:
            self.log('Cleaning up temp files...')
            temp_files = glob.glob('../resources/logs/*') + glob.glob('../resources/images/*') + glob.glob(
                '../resources/result/*')
            for file in temp_files:
                os.remove(file)

    def preprocess(self):
        self.log('Preprocessing image files...')

        i = 0
        temp_files = os.listdir(self.input_path)
        self.log('Found %d files in directory. Filtering...' % len(temp_files))

        for file in temp_files:
            if file.endswith(".png"):
                try:
                    image = Image.open(os.path.join(self.input_path, file))
                    image = crop_image(image)
                    image = pad_image(image)
                    image = scale_image(image, self.downscale)

                    if image.size[0] <= MAX_WIDTH and image.size[1] <= MAX_HEIGHT:
                        image.save('../resources/images/input%d.png' % i)
                        self.file_name_dict['input%d.png' % i] = file
                        i += 1

                except IOError:
                    continue

        self.log('%d files were preprocessed' % i)
        return i


class App(QtWidgets.QMainWindow, design.Ui_MainWindow):
    def __init__(self):
        super(App, self).__init__()
        self.worker = None
        self.input_path = ''
        self.output_path = ''
        self.method = Method.Im2Markup
        self.downscale = DEFAULT_SCALE

        self.setupUi(self)
        self.downscale_coef.setValue(DEFAULT_SCALE)
        self.downscale_coef.valueChanged.connect(self.on_downscale_coefficient_change)
        self.choose_directory.clicked.connect(self.on_choose_input_directory)
        self.choose_output.clicked.connect(self.on_choose_output_directory)
        self.start_processing.clicked.connect(self.start)
        self.choose_method.currentTextChanged.connect(self.on_method_changed)

        for method in Method:
            self.choose_method.addItem(method.name)

        self.log('Choose directory to start')

    def log(self, text):
        self.output.addItem('[' + str(datetime.datetime.now()) + '] ' + text)
        self.output.scrollToBottom()

    def set_editable(self, st):
        self.start_processing.setEnabled(st)
        self.choose_directory.setEnabled(st)
        self.choose_output.setEnabled(st)
        self.choose_method.setEnabled(st)
        self.downscale_coef.setEnabled(st)

    def on_downscale_coefficient_change(self, value):
        self.downscale = value
        self.log('Current downscale coefficient is %f' % self.downscale)

    def on_method_changed(self, value):
        self.method = Method[value]
        self.log('Current method is %s' % self.method.name)

    def on_choose_input_directory(self):
        self.input_path = QtWidgets.QFileDialog.getExistingDirectory(self, "Choose image directory")
        if self.input_path:
            self.log('Image directory is %s' % self.input_path)
            if self.output_path:
                self.start_processing.setEnabled(True)
        else:
            self.log('Current directory is not selected')
            self.start_processing.setEnabled(False)

    def on_choose_output_directory(self):
        self.output_path = QtWidgets.QFileDialog.getExistingDirectory(self, "Choose where to save output file")
        if self.output_path:
            self.log('Output directory is %s' % self.output_path)
            if self.input_path:
                self.start_processing.setEnabled(True)
        else:
            self.log('Output directory is not selected')
            self.start_processing.setEnabled(False)

    def start(self):
        self.worker = Worker(self.method, self.input_path, self.output_path, self.downscale)
        self.worker.write_to_log_signal.connect(self.log)
        self.worker.set_editable_signal.connect(self.set_editable)
        self.worker.start()


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = App()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
